#!/usr/bin/python
import time
import RPi.GPIO as GPIO

#pins
triggerLeft=12
echoLeft=11
triggerStraight=32
echoStraight=31
triggerRight=16
echoRight=15

def init():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(triggerLeft,GPIO.OUT)
	GPIO.setup(echoLeft,GPIO.IN)
	GPIO.setup(triggerStraight,GPIO.OUT)
	GPIO.setup(echoStraight,GPIO.IN)
	GPIO.setup(triggerRight,GPIO.OUT)
	GPIO.setup(echoRight,GPIO.IN)

	GPIO.output(triggerLeft, GPIO.LOW)
	GPIO.output(triggerStraight, GPIO.LOW)
	GPIO.output(triggerRight, GPIO.LOW)

	# Allow module to settle
	time.sleep(2)
  
def measure(direction):
	if direction == "left":
		trigger = triggerLeft
		echo = echoLeft
	else if direction == "right":
		trigger = triggerRight
		echo = echoRight
	else:
		trigger = triggerStraight
		echo = echoStraight

	#print "Send Trigger"
	# Send 10us pulse to trigger
	GPIO.output(trigger, GPIO.HIGH)
	time.sleep(0.00001)
	GPIO.output(trigger, GPIO.LOW)
	while GPIO.input(echo)==0:
		start = time.time()

	while GPIO.input(echo)==1:
		stop = time.time()

	# Calculate pulse length
	elapsed = stop-start
	# Distance pulse travelled in that time is time
	# multiplied by the speed of sound (cm/s)
	distance = elapsed * 17150

	#if distance > 0:
	print " %s : %f cm" % (direction, distance)

	return distance;

def straightClear():
	return measure("straight") > 5

def leftClear():
	return measure("left") > 5

def rightClear():
	return measure("right") > 5

def terminate():
	GPIO.output(triggerLeft, GPIO.LOW)
	GPIO.output(triggerStraight, GPIO.LOW)
	GPIO.output(triggerRight, GPIO.LOW)
