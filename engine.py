#!/usr/bin/python
import sys
import time
import RPi.GPIO as GPIO

frequency = 30
dutyCycle = 25
motor1A = 40
motor1B = 38
motor1E = 36

def init():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(motor1A,GPIO.OUT)
	GPIO.setup(motor1B,GPIO.OUT)
	GPIO.setup(motor1E,GPIO.OUT)
	GPIO.output(motor1E,GPIO.LOW)
	pwm = GPIO.PWM(motor1E,frequency)
	pwm.start(dutyCycle)

def forward():
	GPIO.output(motor1A,GPIO.HIGH)
	GPIO.output(motor1B,GPIO.LOW)
	GPIO.output(motor1E,GPIO.HIGH)

def backward():
	GPIO.output(motor1A,GPIO.LOW)
	GPIO.output(motor1B,GPIO.HIGH)
	GPIO.output(motor1E,GPIO.HIGH)

def stop():
	GPIO.output(motor1E,GPIO.LOW)

def terminate():
	stop()
	pwm.stop()
