#!/usr/bin/python
import sys
import time
import RPi.GPIO as GPIO

# Pulses per second
frequency = 30
# Duty cycle
min = 25 
max = 25
runtime = 2
# pins
motor1A = 40
motor1B = 38
motor1E = 36

try:
	GPIO.setmode(GPIO.BOARD)
	#print "setup motor1A on pin {}".format(motor1A)
	GPIO.setup(motor1A,GPIO.OUT)
	#print "setup motor1B on pin {}".format(motor1B)
	GPIO.setup(motor1B,GPIO.OUT)
	#print "setup motor1E on pin {}".format(motor1E)
	GPIO.setup(motor1E,GPIO.OUT)

	print "setup pwm frequency = {}".format(frequency)
	pwm = GPIO.PWM(motor1E,frequency)   ## pwm de la pin 22 a une frequence de 50 Hz
	#print "start pwm"
	pwm.start(min)   ## on commemnce avec un rapport cyclique de max %

	print "Marche avant sens direct, vitesse (rapport cyclique {} %)".format(min)
	print "motor1A high"
	GPIO.output(motor1A,GPIO.HIGH)
	print "motor1B low"
	GPIO.output(motor1B,GPIO.LOW)
	print "motor1E high"
	GPIO.output(motor1E,GPIO.HIGH)

	#print "sleep 5s"
	time.sleep(runtime)
	
	#print "ChangeDutyCycle to %".format(min)
	#pwm.ChangeDutyCycle(min)

	#print "Rotation sens direct, au ralenti"

	#time.sleep(runtime)	

	#print "Rotation sens inverse, au ralenti"
	#GPIO.output(motor1A,GPIO.LOW)
	#GPIO.output(motor1B,GPIO.HIGH)

	#time.sleep(runtime)

	#pwm.ChangeDutyCycle(max)
	#print"Rotation sens inverse, vitesse maximale (rapport cyclique {} %)".format(max) 
	#time.sleep(runtime)


	print "Arret du moteur"
	GPIO.output(motor1E,GPIO.LOW)

	pwm.stop()    ## interruption du pwm

except:
	print "Unexpected error:", sys.exc_info()[0]
finally:
	GPIO.cleanup()
