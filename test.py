import time
import RPi.GPIO as GPIO

port=7
upTime=10

GPIO.setmode(GPIO.BOARD)
GPIO.setup(port,GPIO.OUT)
GPIO.output(port,True)
print "Port {} on {}s".format(port,upTime)
time.sleep(upTime)
GPIO.output(port,False)
print "Port {} off".format(port)
GPIO.cleanup()
