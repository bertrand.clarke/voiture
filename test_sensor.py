#!/usr/bin/python
import time
import RPi.GPIO as GPIO

#pins
triggerLeft=12
echoLeft=11
triggerStraight=32
echoStraight=31
triggerRight=16
echoRight=15

def mesure(direction, trigger, echo ):
	#print "Send Trigger"
	# Send 10us pulse to trigger
	GPIO.output(trigger, True)
	time.sleep(0.00001)
	GPIO.output(trigger, False)
	while GPIO.input(echo)==0:
		start = time.time()

	while GPIO.input(echo)==1:
		stop = time.time()

	# Calculate pulse length
	elapsed = stop-start
	# Distance pulse travelled in that time is time
	# multiplied by the speed of sound (cm/s)
	distance = elapsed * 17150

	#if distance > 0:
	print " %s : %f cm" % (direction, distance)

	return;

print "Distance Measurement In Progress"

GPIO.setmode(GPIO.BOARD)
GPIO.setup(triggerLeft,GPIO.OUT)
GPIO.setup(echoLeft,GPIO.IN)
GPIO.setup(triggerStraight,GPIO.OUT)
GPIO.setup(echoStraight,GPIO.IN)
GPIO.setup(triggerRight,GPIO.OUT)
GPIO.setup(echoRight,GPIO.IN)

# Set trigger to False (Low)
GPIO.output(triggerLeft, False)
GPIO.output(triggerStraight, False)
GPIO.output(triggerRight, False)

print "Waiting For Sensor To Settle"

# Allow module to settle
time.sleep(2)

try:
	while True:
		mesure("left", triggerLeft, echoLeft)
		mesure("center", triggerStraight, echoStraight)
		mesure("right", triggerRight, echoRight)
		time.sleep(3)
	GPIO.cleanup()
except:
       	GPIO.cleanup()
	exit()


