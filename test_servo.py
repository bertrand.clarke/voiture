#!/usr/bin/python
import time
import sys
import RPi.GPIO as GPIO


try:
	frequency = 60
	servo_pin = 13 
     
	GPIO.setmode(GPIO.BOARD)

	GPIO.setup(servo_pin, GPIO.OUT)

	GPIO.output(servo_pin, GPIO.HIGH)

	pwm = GPIO.PWM(servo_pin, frequency)

	rapport = 6.0   # rapport cyclique initial de 7%

	pwm.start(rapport)  

	while True:
		print "Rapport cyclique actuel: " , rapport
		rapport = raw_input ("Nouveau rapport cyclique (%):  ")
		pwm.ChangeDutyCycle(float(rapport))
except:
        print "Unexpected error:", sys.exc_info()[0]
finally:
	pwm.stop()
        GPIO.cleanup()
