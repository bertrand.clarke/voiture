import time 
import RPi.GPIO as GPIO

port=7

GPIO.setmode(GPIO.BOARD)
GPIO.setup(port,GPIO.OUT)

def blink():
	while True:
		GPIO.output(port,True)
		print "Led on" 
		time.sleep(1)
		print "Led off" 
		GPIO.output(port,False)
		time.sleep(1)
	return

try:
	blink()
except KeyboardInterrupt:
	GPIO.cleanup()
