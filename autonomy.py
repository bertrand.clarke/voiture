#!/usr/bin/python
import sys
import time

import sensor
import engine
import servo

try:
	sensor.init()
	servo.init()
	engine.init()
  
	while True:
		if sensor.straightClear():
			engine.forward()  
		while sensor.straightClear():
			time.sleep(0.2)
		if sensor.leftClear():
			servo.left()
			time.sleep(0.2)	
			servo.forward()	
		else if sensor.rightClear():
			servo.right()
			time.sleep(0.2)	
			servo.forward()	
		else:
			engine.stop()
			servo.left()
			engine.backward()
			time.sleep(0.2)
			engine.stop()

except:
	print "Unexpected error:", sys.exc_info()[0]
finally:
	engine.terminate()
	servo.terminate()
	sensor.terminate()
	GPIO.cleanup()
