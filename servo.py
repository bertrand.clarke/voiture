#!/usr/bin/python
import sys
import time
import RPi.GPIO as GPIO

frequency = 60
servo = 13
straightRatio = 6.0
leftRatio = 8.0
rightRatio = 4.0

def init():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(servo, GPIO.OUT)
	GPIO.output(servo, GPIO.HIGH)
	pwm = GPIO.PWM(servo, frequency)
	pwm.start(straightRatio)

def left():
	pwm.ChangeDutyCycle(leftRatio)  

def right():
	pwm.ChangeDutyCycle(rightRatio)  

def straight():
	pwm.ChangeDutyCycle(straightRatio)  

def terminate():
	GPIO.output(servo, GPIO.LOW)
	pwm.stop()
